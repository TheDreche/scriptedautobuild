#!/bin/sh

SELF="$0"

isn() {
	return "$1"
}

is() {
	if isn "$1"; then
		return 1
	else
		return 0
	fi
}

fail() {
	echo "$2" >&2
	exit "$1"
}

isProjectRoot() {
	test -f configure.ac
	return $?
}

# Default options
dir_get_project_root() {
	(
		if test -n "$SRCDIR"; then
			cd "$SRCDIR"
			if is "$?"; then
				if isProjectRoot; then
					pwd
					exit 0
				fi
			fi
		fi
		exit 1
	) || (
		dir_prev=""
		while ! test "$dir_prev" = "$(pwd)"; do
			if isProjectRoot; then
				pwd
				exit 0
			fi
			dir_prev="$(pwd)"
			cd ..
		done
		exit 1
	) || (
		dir_prev=""
		case "$SELF" in
			/) cd / || exit 1;;
			*/*/) cd "${SELF%/*/}" || exit 1;;
			*/*) cd "${SELF%/*}" || exit 1;;
		esac
		while ! test "$dir_prev" = "$(pwd)"; do
			if isProjectRoot; then
				pwd
				exit 0
			fi
			dir_prev="$(pwd)"
			cd ..
		done
		exit 1
	)
	return "$?"
}

tool() {
	which "$1"
	return "$?"
}

# Option declaration
do_quiet=0
do_config_cache=0
do_jobs=''

dir_project_root="$(dir_get_project_root)"
tool_autoreconf="$(tool autoreconf)"
tool_make="$(tool make)"
tool_nproc="$(tool nproc)"
tool_sh="$(tool sh)"

configure_options=""

invalid_options=""
invalid_values=""
invalid_tools=""

msg_help() {
	cat << EOF
Usage: $SELF <options> -- <tools> <configure env>

Options include:
  --help, -h           Print this help message
  --config-cache, -C   Save config.cache
  --srcdir=*           Location of the source directory
  --jobs=1             Number of make jobs for compilation
  --quiet, -q          Be less verbose

Options for configure:
  --build=*
  --*dir=*
  --disable-*
  --enable-*
  --exec-prefix=*
  --host=*
  --prefix=*
  --program-prefix=*
  --program-suffix=*
  --program-transform=*
  --with-*
  --without-*

Tools example:
  SH /usr/bin/sh AUTORECONF /usr/bin/autoreconf

Tools needed:
  AUTORECONF
  MAKE
  NPROC
  SH

Configure env is a list of arguments setting environment variables.
Configure env example:
  CC=/usr/bin/cc CPP=/usr/bin/cpp

One deatailed example for all configure options:
  $0 --enable-debug --srcdir=../sources --disable-static --prefix=/inst/project SH /bin/dash CXX=/usr/bin/g++

That would run
  /bin/dash ../sources/configure --enable-debug --srcdir=../sources --disable-static --prefix=/inst/project CXX=/usr/bin/g++
under the hood; the arguments may be in a different order.
EOF
}

configure_opt() {
	tmp="$configure_options"
	configure_options="$(cat << EOF
$tmp
$1
EOF
)"
}

parse_tool() {
	tool="$1"
	path="$2"
	case "$tool" in
		AUTORECONF) tool_autoreconf="$path";;
		MAKE) tool_make="$path";;
		NPROC) tool_nproc="$path";;
		SH) tool_sh="$path";;
		*) invalid_tools="$invalid_tools $tool"
	esac
}

parse_opt_long() {
	opt="$1"
	if test -n "$2"; then
		arg="$2"
		case "$opt" in
			--jobs)
				if test -n "${arg##*[!0-9]*}"; then
					do_jobs="$arg"
				else
					invalid_values="$invalid_values $arg=$opt"
				fi;;
			--srcdir) dir_project_root="$opt";;
			--*dir|--build|--exec-prefix|--prefix|--host|--program-prefix|--program-suffix|--program-transform) configure_opt "$arg=$opt";;
			*) return 1
		esac
		return 0
	fi
	case "$opt" in
		--help) msg_help; exit 0;;
		--quiet=y|--quiet=yes|--quiet) do_quiet=1;;
		--quiet=n|--quiet=no) do_quiet=0;;
		--quiet=*) invalid_values="$invalid_values $opt";;
		--config-cache=y|--config-cache=yes|--config-cache) do_config_cache=1;;
		--config-cache=n|--config-cache=no) do_config_cache=0;;
		--config-cache=*) invalid_values="$invalid_values $opt";;
		--jobs=*)
			if test -n "${opt##--jobs=*[!0-9]*}"; then
				do_jobs="${opt#--jobs=}"
			else
				invalid_values="$invalid_values $opt"
			fi;;
		--srcdir=*) dir_project_root="${opt#--srcdir=}";;
		--build=*|--*dir=*|--disable-*|--enable-*|--exec-prefix=*|--prefix=*|--host=*|--program-prefix=*|--program-suffix=*|--program-transform=*|--with-*|--without-*) configure_opt "$opt";;
		--build|--*dir|--exec-prefix|--prefix|--host|--program-prefix|--program-suffix|--program-transform|--jobs) expected_arg=1;;
		*) invalid_options="$invalid_options $opt";;
	esac
}

parse_opt_short() {
	opt="$1"
	case "$opt" in
		h) msg_help; exit 0;;
		q) do_quiet=1;;
		C) do_config_cache=1;;
		*) invalid_options="$invalid_options -$opt"
	esac
}

canbeopt=1
parsing_tool=""
expected_arg=0
for arg; do
	if is "$expected_arg"; then
		expected_arg=0
		case "$parsing_tool" in
			--*) parse_opt_long "$parsing_tool" "$arg";;
			*) parse_tool "$parsing_tool" "$arg"
		esac
	elif is "$canbeopt"; then
		case "$arg" in
			--|-) canbeopt=0;;
			--*) parse_opt_long "$arg";;
			-*)	argcpy="${arg#-}"
				while test -n "$argcpy"; do
					parse_opt_short "$(printf "%.1s\n" "$argcpy")"
					argcpy="${argcpy#?}"
				done;;
			*=*) parse_conf_env "$arg";;
			*) expected_arg=1;;
		esac
	else
		case "$arg" in
			*=*) parse_conf_env "$arg";;
			*/*) tags_dir "$arg";;
			*) expected_arg=1;;
		esac
	fi
	if is "$expected_arg"; then
		parsing_tool="$arg"
	fi
done

wrongargs=0
if is "$expected_arg"; then
	wrongargs=1
	case "$parsing_tool" in
		--*) echo "Missing argument for option $parsing_tool";;
		*) echo "Missing argument for tool $parsing_tool";;
	esac
	echo ''
fi
if test -n "$invalid_options"; then
	wrongargs=1
	echo 'Invalid options:'
	echo "$invalid_options"
	echo ''
fi
if test -n "$invalid_values"; then
	wrongargs=1
	echo 'Invalid options for arguments:'
	echo "$invalid_options"
	echo ''
fi
if test -n "$invalid_tools"; then
	wrongargs=1
	echo 'Invalid tools:'
	echo "$invalid_tools"
	echo ''
fi
if is "$wrongargs"; then
	echo "For help use $0 --help"
fi

if test -z "$do_jobs"; then
	do_jobs="$("$tool_nproc")"
	if test -z "${do_jobs##*[!0-9]*}"; then
		do_jobs=1
	fi
fi

# Actions
run() {
	echo ">>> $*"
	"$@"
	return "$?"
}

run_args() {
	if read -r arg; then
		run_args "$@" "$arg"
	else
		run "$@"
	fi
}

test -z "$dir_project_root" && fail 1 '+++ Could not find project root'
cd "$dir_project_root" || fail "$?" '+++ Could not switch to project root'

if ! test -f Makefile; then
	if is "$do_quiet"; then
		run "$tool_autoreconf" --install || fail "$?" '+++ Command failed'
	else
		run "$tool_autoreconf" --install --verbose || fail "$?" '+++ Command failed'
	fi
	run_args "$tool_sh" << EOF
./configure
$configure_options
--srcdir=$dir_project_root
EOF
	if ! (exit "$?"); then
		fail "$?" '+++ Configure failed'
	fi
fi
opt_quiet=""
if is "$do_quiet"; then
	opt_quiet="--quiet"
fi
run "$tool_make" --jobs="$do_jobs" $opt_quiet Makefile || fail "$?" '+++ make Makefile failed'
run "$tool_make" --jobs="$do_jobs" $opt_quiet maintainer-clean || fail "$?" '+++ cleaning failed'
if is "$do_quiet"; then
	run "$tool_autoreconf" --install || fail "$?" '+++ Command failed'
else
	run "$tool_autoreconf" --install --verbose || fail "$?" '+++ Command failed'
fi

if is "$do_config_cache"; then
	run_args "$tool_sh" << EOF
./configure
$configure_options
$opt_quiet
--srcdir=$dir_project_root
EOF
	if ! (exit "$?"); then
		fail "$?" '+++ Configure failed'
	fi
else
	run_args "$tool_sh" << EOF
./configure
$configure_options
$opt_quiet
-C
--srcdir=$dir_project_root
EOF
	if ! (exit "$?"); then
		fail "$?" '+++ Configure failed'
	fi
fi
run "$tool_make" $opt_quiet distcheck
