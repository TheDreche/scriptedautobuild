#!/bin/sh

self="$0"

is() {
	if test "$1" -eq 0; then
		return 1
	else
		return 0
	fi
}

isn() {
	return "$1"
}

usage() {
	cat << EOF
Usage: $self <options> -- <file>

If no option is given, prints the commit in which <file> was last modified to stdout.

Options:
  --help              Print this message and exit
  --distver=file      Fall back to reading the commit from file
  --update            Update --distver
                      If <file> is provided, only update its state
  --modified=+mod     Check whether <file> is modified and mark using +mod suffix
  --sed=/path/to/sed  Use provided path to sed
  --fallback=unknown  Provide a last fallback. Otherwise exit with status != 0.
  --newline=no        Do not add a trailing newline to output
  --tr=/path/to/tr    Use provided path to tr

<file> has to be a single file, except for when --update is given.
In that case, print nothing to stdout but all files get added/updated.
EOF
}

opt_distver=''
opt_update=0
opt_modified=''
opt_sed='sed'
opt_fallback=''
opt_file=''
opt_newline=1
opt_tr='tr'
multifile=0

canbeopt=1
for arg; do
	have_read=0
	if is "$canbeopt"; then
		have_read=1
		case "$arg" in
			--help) usage && exit 0;;
			--distver=*) opt_distver="${arg#--distver=}";;
			--update|--update=y|--update=yes) opt_update=1;;
			--update=n|--update=no) opt_update=0;;
			--modified=*) opt_modified="${arg#--modified=}";;
			--modified) opt_modified='+modified';;
			--sed=*) opt_sed="${arg#--sed=}";;
			--fallback=*) opt_fallback="${arg#--fallback=}";;
			--fallback) opt_fallback='';;
			--newline=y|--newline=yes|--newline=on|--newline) opt_newline=1;;
			--newline=n|--newline=no|--newline=off) opt_newline=0;;
			--tr=*) opt_tr="${arg#--tr=}";;
			--) canbeopt=0;;
			--*) invalid_options="$invalid_options $arg";;
			*) have_read=0;;
		esac
	fi
	if isn "$have_read"; then
		if test -n "$opt_file"; then
			multifile=1
		fi
		opt_file="$arg"
	fi
done

if test -n "$invalid_options"; then
	echo 'Error: Invalid options/values:' >&2
	echo "$invalid_options" >&2
	exit 1
fi

if test -z "$opt_file"; then
	echo 'Error: No file given' >&2
	exit 1
fi

# Later values override previous ones
result="$opt_fallback"

distveranalyze() {
	if test -z "$opt_distver" || ! test -f "$opt_distver"; then
		distver_result=''
		return
	fi
	escaped_opt_file="$(echo "$1" | "$opt_sed" 's / \\/ g;s \. \\\. g')"
	distver_result="$("$opt_sed" "/^[^ ]\+ $escaped_opt_file\$/s ^ _ ;/[^_]/d;s/^_[^ ]\+ \(.*\)$/\1/" "$opt_distver")"
}

if test -n "$opt_distver"; then
	distveranalyze "$opt_file"
	if test -n "$distver_result"; then
		result="$distver_result"
	fi
fi

distverupdate() {
	touch "$opt_distver"
	escaped_opt_file="$(echo "$1" | "$opt_sed" 's / \\/ g;s \. \\\. g')"
	if test -z "$("$opt_sed" "s/^[^ ]\+ $escaped_opt_file/_&/;/^[^_]/d" "$opt_distver")"; then
		echo "$2 $1" >> "$opt_distver"
	else
		"$opt_sed" -i "s/[^ ]+ $escaped_opt_file\$/$2 $escaped_opt_file/" "$opt_distver"
	fi
}

gitanalyze() {
	git_analyze_file="$1"
	git_result="$(git rev-list --max-count=1 HEAD -- "$git_analyze_file")"
	if test -z "$git_result"; then
		# File is not recognized by git
		git_result=''
		git_modified=0
	else
		if ! git diff --quiet -- "$git_analyze_file"; then
			git_modified=1
		else
			git_modified=0
		fi
	fi
}

if test -d .git; then
	# git found! Success!
	if which git >/dev/null 2>/dev/null; then
		if isn "$multifile"; then
			gitanalyze "$opt_file"
			if test -n "$git_result"; then
				if is "$git_modified"; then
					result="$git_result$opt_modified"
				else
					result="$git_result"
				fi
			fi
			if is "$opt_update"; then
				distverupdate "$opt_file" "$git_result"
			fi
		else
			canbeopt=1
			for f; do
				used=0
				if is "$canbeopt"; then
					used=1
					case "$f" in
						--) canbeopt=0;;
						--*) ;;
						*) used=0;;
					esac
				fi
				if isn "$used"; then
					gitanalyze "$f"
					if test -n "$git_result"; then
						distverupdate "$f" "$git_result"
					fi
				fi
			done
		fi
	fi
fi

if is "$multifile"; then
	exit 0
elif test -z "$result"; then
	exit 1
else
	if is "$opt_newline"; then
		echo "$result"
	else
		echo "$result" | "$opt_tr" -d '\n'
	fi
	exit 0
fi
