#!/bin/sh

SELF="$0"

isn() {
	return "$1"
}

is() {
	if isn "$1"; then
		return 1
	else
		return 0
	fi
}

isProjectRoot() {
	test -f configure.ac || test -f config.status
	return $?
}

dir_get_project_root() {
	(
		if test -n "$SRCDIR"; then
			cd "$SRCDIR"
			if is "$?"; then
				if isProjectRoot; then
					pwd
					exit 0
				fi
			fi
		fi
		exit 1
	) || (
		dir_prev="$(pwd)"
		while ! test "$dir_prev" = "$(pwd)"; do
			dir_prev="$(pwd)"
			cd .. || exit 1
			if isProjectRoot; then
				pwd
				exit 0
			fi
		done
		exit 1
	) || (
		cd "$SELF" || exit 1
		if isProjectRoot; then
			pwd
			exit 0
		fi
		exit 1
	) || (
		dir_prev=""
		case "$SELF" in
			/) cd / || exit 1;;
			*/*/) cd "${SELF%/*/}" || exit 1;;
			*/*) cd "${SELF%/*}" || exit 1;;
		esac
		while ! test "$dir_prev" = "$(pwd)"; do
			if isProjectRoot; then
				pwd
				exit 0
			fi
			dir_prev="$(pwd)"
			cd ..
		done
	)
	return "$?"
}

dir_project_root="$(dir_get_project_root)"
dir_checking="$dir_project_root/_checking"
dir_tars="$dir_checking/tars"

# Parse options
help_msg() {
	cat << EOF
Usage: $0 <options> <tools>

Options include:
  --help, -h                   Print this help message
  --auto-distcheck=<no|yes>    Whether to do make distcheck
  --auto-robuild=<yes|no>      Whether to build with read-only sources
  --auto-check=<yes|no>        Whether to do make check
  --auto-install=<yes|no>      Whether to install
  --auto-installcheck=<yes|no> Whether to do make installcheck
  --cmake=<yes|no>             Whether to do all of cmake
  --cmp-installation=<yes|no>  Whether to compare CMake and automake installation
  --cleanup=<yes|late|no>      Whether to clean up the folder containing everything
  --restore=<yes|no>           Whether to restore old configuration

Options starting with --auto- are using GNU build system.
The first value is the default.

Example for tools:
  CMAKE /usr/bin/cmake MAKE /usr/bin/make

Tools needed are:
  AUTORECONF
  MAKE
  SH
  CMAKE
  CP
  MV
  MKDIR
  RM
  TAR
EOF
}

configure_options="--enable-debug"

tool_autoreconf="$(which autoreconf)"
tool_make="$(which make)"
tool_sh="$(which sh)"
tool_cmake="$(which cmake)"
tool_cp="$(which cp)"
tool_mv="$(which mv)"
tool_mkdir="$(which mkdir)"
tool_rm="$(which rm)"
tool_tar="$(which tar)"
tool_chmod="$(which chmod)"

do_auto_distcheck=0
do_auto_robuild=1
do_auto_check=1
do_auto_install=1
do_auto_installcheck=1
do_cmake=1
do_cmp_installation=1

# cleanup is one of yes, late or no
cleanup=yes
restore=1

invalid_options=""
invalid_values=""
invalid_tools=""

parse_opt_long() {
	case "$1" in
		--help) help_msg; exit 0;;
		--auto-distcheck=yes|--auto-distcheck) do_auto_distcheck=1;;
		--auto-distcheck=no) do_auto_distcheck=0;;
		--auto-distcheck=*) invalid_values="$invalid_values $1";;
		--auto-robuild=yes|--auto-robuild) do_auto_robuild=1;;
		--auto-robuild=no) do_auto_robuild=0;;
		--auto-robuild=*) invalid_values="$invalid_values $1";;
		--auto-check=yes|--auto-check) do_auto_check=1;;
		--auto-check=no) do_auto_check=0;;
		--auto-check=*) invalid_values="$invalid_values $1";;
		--auto-install=yes|--auto-install) do_auto_install=1;;
		--auto-install=no) do_auto_install=0;;
		--auto-install=*) invalid_values="$invalid_values $1";;
		--auto-installcheck=yes|--auto-installcheck) do_auto_installcheck=1;;
		--auto-installcheck=no) do_auto_installcheck=0;;
		--auto-installcheck=*) invalid_values="$invalid_values $1";;
		--cmake=yes|--cmake) do_cmake=1;;
		--cmake=no) do_cmake=0;;
		--cmake=*) invalid_values="$invalid_values $1";;
		--cmp-installation=yes|--cmp-installation) do_cmp_installation=1;;
		--cmp-installation=no) do_cmp_installation=0;;
		--cmp-installation=*) invalid_values="$invalid_values $1";;
		--cleanup=yes|--cleanup) cleanup=yes;;
		--cleanup=late) cleanup=late;;
		--cleanup=no) cleanup=no;;
		--cleanup=*) invalid_values="$invalid_values $1";;
		--restore=yes|--restore) restore=1;;
		--restore=no) restore=0;;
		--restore=*) invalid_values="$invalid_values $1";;
		*) invalid_options="$invalid_options $1";;
	esac
}

parse_opt_short() {
	echo "Short $1"
	case "$1" in
		h) help_msg; exit 0;;
		*) invalid_options="$invalid_options -$1";;
	esac
}

parse_tool() {
	case "$1" in
		SH) tool_sh="$2";;
		MAKE) tool_make="$2";;
		AUTORECONF) tool_autoreconf="$2";;
		CMAKE) tool_cmake="$2";;
		CP) tool_cp="$2";;
		MV) tool_mv="$2";;
		MKDIR) tool_mkdir="$2";;
		RM) tool_rm="$2";;
		TAR) tool_tar="$2";;
		CHMOD) tool_chmod="$2";;
		*) invalid_tools="$invalid_tools $2";;
	esac
}

canbeopt=1
parsing_tool=""
for arg; do
	if is "$canbeopt"; then
		case "$arg" in
			--|-) canbeopt=0;;
			--*) parse_opt_long "$arg";;
			-*)	argcpy="$arg"
				while test -n "$argcpy"; do
					parse_opt_short "$(printf "%.1s\n" "$argcpy")"
				done;;
			*) canbeopt=0; parsing_tool="$arg";;
		esac
	else
		if test -z "$parsing_tool"; then
			parsing_tool="$arg"
		else
			parse_tool "$parsing_tool" "$arg"
			parsing_tool=""
		fi
	fi
done
wrongargs=0
if test -n "$invalid_options"; then
	echo 'Invalid options:'
	echo "$invalid_options"
	echo ''
	wrongargs=1
fi
if test -n "$invalid_values"; then
	echo 'Invalid argument values:'
	echo "$invalid_values"
	echo ''
	wrongargs=1
fi
if test -n "$parsing_tool"; then
	echo "Missing path for tool $parsing_tool"
	echo ''
	wrongargs=1
fi
if test -n "$invalid_tools"; then
	echo 'Invalid tool names:'
	echo "$invalid_tools"
	echo ''
	wrongargs=1
fi
if is "$wrongargs"; then
	echo "Use $0 --help for help!"
	exit 1
fi

run() {
	echo ">>> $*"
	"$@"
	return $?
}

# Preparations
# $restore is checked at 2 locations
if is "$restore" && test -f ./config.status; then
	run "$tool_cp" config.status config.status_bak
fi

if test -f Makefile; then
	run "$tool_make" distclean
elif test -f config.status; then
	run "$tool_sh" ./config.status --file=Makefile
	run "$tool_make" distclean
elif ! test -f Makefile.in; then
	run "$tool_autoreconf" --install --verbose
fi

full_cleanup() {
	did_cd=1
	other_exit_code=0
	if ! cd "$dir_project_root"; then
		echo '+++ Could not switch to project root!' >&2
		other_exit_code="$?"
		did_cd=0
		cd /
	fi
	if test "$cleanup" = yes || test "$cleanup" = late; then
		echo '### Cleaning up artifacts ...'
		run "$tool_chmod" u+rw --recursive -- "$dir_checking" || echo '+++ Error at cleaning up: Could not change permission' >&2
		if ! run "$tool_rm" -rf -- "$dir_checking"; then
			other_exit_code="$?"
			echo '+++ Could not delete artifacts!' >&2
			echo "+++ Artifacts are in $dir_checking"
		fi
	fi
	
	# Restore old configuration
	# $restore is checked at 2 locations
	if is "$did_cd" && is "$restore" && test -f config.status_bak; then
		echo '### Restoring previous configuration ...'
		if run "$tool_mv" config.status_bak config.status; then
			if ! run "$tool_sh" ./config.status --recheck; then
				tmp="$?"
				if "$other_exit_code" -eq 0; then
					other_exit_code="$tmp"
				fi
				echo '+++ Could not recheck!' >&2
			fi
		else
			tmp="$?"
			if "$other_exit_code" -eq 0; then
				other_exit_code="$tmp"
			fi
			echo '+++ Could not restore config.status!' >&2
			echo 'Original config.status is not called config.status_bak'
		fi
	fi

	if test "$cleanup" = no; then
		echo "+++ Artifacts are in $dir_checking"
	fi
	
	if test "$1" -eq 0; then
		exit "$other_exit_code"
	fi
	
	exit "$1"
}

dir_setup() {
	run "$tool_mkdir" -- "$dir_checking/$1" || return
	cd "$dir_checking/$1" || return
}

dir_cleanup() {
	cd "$dir_checking" || return
	if test "$cleanup" = yes; then
		run "$tool_chmod" u+rw --recursive -- "$1" || return
		run "$tool_rm" -rf -- "$1" || return
	fi
}

tar_is() {
	test -f "$dir_tars/$1"
	return "$?"
}

tar_generate() {
	run "$tool_chmod" --recursive u+rw -- "$1" || return
	run "$tool_tar" -cf "$dir_tars/$2" -- "$1" || return
}

tar_use() {
	dir_orig="$(pwd)"
	run "$tool_mkdir" -p -- "$dir_orig/$2" || return
	dir_dirchk="$dir_checking/dirchk"
	run "$tool_mkdir" -p -- "$dir_dirchk" || return
	if ! (
		cd "$dir_dirchk" || exit
		run "$tool_tar" -xf "$dir_tars/$1" || exit
	); then
		x="$?"
		run "$tool_rm" -rf -- "$dir_dirchk" || echo "While cleaning, an error occurred: $?"
		return "$x"
	fi
	run "$tool_mv" -- "$dir_dirchk"/*/* "$dir_orig/$2" || return
	run "$tool_chmod" u+rw --recursive "$dir_dirchk" || return
	run "$tool_rm" -rf -- "$dir_dirchk" || return
}

tar_gen_autodist() {
	if ! tar_is autodist.tar; then
		echo '### Preparing distributable sources ...'
		dir_setup autodist_create || return
		run "$tool_sh" "$dir_project_root/configure" $configure_options --srcdir="$dir_project_root" || return
		run "$tool_make" distdir || return
		tar_generate devent-* autodist.tar || return
		dir_cleanup autodist_create || return
	fi
}

tar_gen_autorobuild() {
	if ! tar_is autorobuild.tar; then
		if ! tar_is autodist.tar; then
			tar_gen_autodist || return
		fi
		
		echo '### Building ...'
		dir_setup autorobuild || return
		tar_use autodist.tar src || return
		run "$tool_mkdir" -p -- build || return
		
		dir_work="$(pwd)"
		dir_src="$dir_work/src"
		dir_build="$dir_work/build"
		
		run "$tool_chmod" --recursive u-w -- src
		
		if ! (
			cd build || exit
			run "$tool_sh" "$dir_src/configure" $configure_options --srcdir="../src" || exit
			run "$tool_make" || exit
		); then
			return "$?"
		fi
		
		tar_generate build autorobuild.tar || return
		dir_cleanup autorobuild || return
	fi
}

tar_gen_autoinstall() {
	if ! tar_is autoinstall.tar; then
		if ! tar_is autodist.tar; then
			tar_gen_autodist || return
		fi
		if ! tar_is autorobuild.tar; then
			tar_gen_autorobuild || return
		fi
		
		echo '### Installing ...'
		dir_setup autoinstall || return
		tar_use autodist.tar src || return
		tar_use autorobuild.tar build || return
		run "$tool_mkdir" -p -- inst || return
		
		dir_work="$(pwd)"
		dir_src="$dir_work/src"
		dir_build="$dir_work/build"
		dir_inst="$dir_work/inst"
		
		if ! (
			cd build || exit
			run "$tool_make" DESTDIR="$dir_inst" install || exit
		); then
			return "$?"
		fi
		
		tar_generate inst autoinstall.tar || return
		dir_cleanup autoinstall || return
	fi
}

auto_distcheck() {
	if is "$do_auto_distcheck"; then
		do_auto_distcheck=0
		
		if ! tar_is autodist.tar; then
			tar_gen_autodist || return
		fi
		
		echo '### Checking distribution ...'
		dir_setup auto_distcheck || return
		tar_use autodist.tar src || return
		run "$tool_mkdir" -p -- build || return
		
		dir_work="$(pwd)"
		dir_src="$dir_work/src"
		dir_build="$dir_work/build"
		
		if ! (
			cd "$dir_build" || exit
			run "$tool_sh" "$dir_src/configure" $configure_options --srcdir="$dir_src" || exit
			run "$tool_make" distcheck || exit
		); then
			return "$?"
		fi
		dir_cleanup auto_distcheck || return
	fi
}

auto_robuild() {
	if is "$do_auto_robuild"; then
		do_auto_robuild=0
		tar_gen_autorobuild || return
	fi
}

auto_check() {
	if is "$do_auto_check"; then
		do_auto_check=0
		
		if ! is_tar autodist.tar; then
			tar_gen_autodist || return
		fi
		if ! is_tar autorobuild.tar; then
			tar_gen_autorobuild || return
		fi
		
		echo '### Checking build ...'
		dir_setup autocheck || return
		tar_use autodist.tar src || return
		tar_use autorobuild.tar build || return
		
		if ! (
			cd build || exit
			run "$tool_make" check
		); then
			return "$?"
		fi
		
		dir_cleanup autocheck || return
	fi
}

auto_install() {
	if is "$do_auto_install"; then
		do_auto_install=0
		tar_gen_autoinstall || return
	fi
}

auto_installcheck() {
	if is "$do_auto_installcheck"; then
		do_auto_installcheck=0
		
		if ! is_tar autodist.tar; then
			tar_gen_autodist || return
		fi
		if ! is_tar autorobuild.tar; then
			tar_gen_autorobuild || return
		fi
		
		echo '### Checking installation ...'
		dir_setup autoinstcheck || return
		tar_use autodist.tar src || return
		tar_use autorobuild.tar build || return
		
		if ! (
			cd build || exit
			run "$tool_make" installcheck
		); then
			return "$?"
		fi
		
		dir_cleanup autoinstcheck || return
	fi
}

tar_gen_cmakedist() {
	if ! tar_is cmakedist.tar; then
		echo '### Copying sources for CMake ...'
		echo '*Autotools: make dist*'
		dir_setup cmakedist_create || return
		run "$tool_mkdir" -p -- allsrcs || return
		
		if ! (
			cd allsrcs || exit
			for f in "$dir_project_root/"*; do
				if ! test "$f" = "$dir_checking"; then
					run "$tool_cp" --recursive -- "$f" .
				fi
			done
		); then
			full_cleanup "$?"
		fi
		
		tar_generate allsrcs cmakedist.tar || return
		dir_cleanup cmakedist_create || return
	fi
}

tar_gen_cmakeinst() {
	if ! tar_is cmakeinst.tar; then
		if ! tar_is cmakedist.tar; then
			tar_gen_cmakedist || return
		fi
		
		echo '### (CMake) Building ...'
		dir_setup cmakeinst || return
		tar_use cmakedist.tar src || return
		run "$tool_mkdir" -p -- build || return
		run "$tool_mkdir" -p -- inst || return
		
		dir_work="$(pwd)"
		dir_src="$dir_work/src"
		dir_build="$dir_work/build"
		
		run "$tool_chmod" --recursive u-w -- src || return
		
		if ! (
			cd build || exit
			run "$tool_cmake" ../src || exit
			run "$tool_cmake" --build . || exit
			run "$tool_make" DESTDIR=../inst install || exit
		); then
			return "$?"
		fi
		
		tar_generate inst cmakeinst.tar || return
		dir_cleanup cmakeinst || return
	fi
}

cmake_inst() {
	if is "$do_cmake"; then
		do_cmake=0
		tar_gen_cmakeinst || return
	fi
}

run "$tool_mkdir" -p -- "$dir_checking" || exit
cd "$dir_checking" || full_cleanup "$?"
run "$tool_mkdir" -p -- "$dir_tars" || full_cleanup "$?"

if is "$do_auto_distcheck"; then
	auto_distcheck || full_cleanup "$?"
fi
if is "$do_auto_robuild"; then
	auto_robuild || full_cleanup "$?"
fi
if is "$do_auto_check"; then
	auto_check || full_cleanup "$?"
fi
if is "$do_auto_install"; then
	auto_install || full_cleanup "$?"
fi
if is "$do_auto_installcheck"; then
	auto_installcheck || full_cleanup "$?"
fi
if is "$do_cmake"; then
	cmake_inst || full_cleanup "$?"
fi
if is "$do_cmp_installation"; then
	# TODO
	true
fi

full_cleanup 0
