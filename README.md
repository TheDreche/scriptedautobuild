# scriptedautobuild

This project is a project containing useful scripts for autotools-based projects.

## Idea

I often found myself needing exactly the same tools over and over in different projects when using the autotools. Since upgrades of these scripts in many repositories became slow quickly, I created this repository. It should contain many useful scripts for the autotools.

## Current status

Some scripts are there already:

- `check.sh` checks whether a project builds correctly. It can do some other things, too!
- `develsetup.sh` sets up some files such as a compilation database and prepares everything for development quickly.
- `diram.sh` generates a file for installing a full directory structure.
- `mkdist.sh` generates a distribution and checks it completely (`make distcheck`). It does some extra steps to get started with distributing quickly (such as `autoreconf --install`)

Run any of these scripts with --help to get info on usage.

## Versioning

This project doesn't use a versioning system. It has no fixed version. Instead, it is intended to just get added new features over time. A complex versioning system would not fit as this is designed to be a git submodule. Scripts not used by end users but by the build system should be backward compatible.
